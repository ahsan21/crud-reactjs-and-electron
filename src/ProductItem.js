import React, { Component } from 'react';

class ProductItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            isEdit: false
        }
    }

    onEdit(){
        this.setState({ isEdit: true })
    }

    onEditSubmit(event){
        event.preventDefault();

        this.props.onEditSubmit(this.nameInput.value, this.priceInput.value, this.props.name);

        this.setState({ isEdit: false })
    }

    onDelete() {
        const { onDelete, name } = this.props;

        onDelete(name)
    }


  render() {
      const { name, price } = this.props;
    return (
      <div>
          {
              this.state.isEdit
              ? (
                <form onSubmit={this.onEditSubmit.bind(this)}>
                    <input 
                        placeholder="Name" 
                        ref={nameInput => this.nameInput = nameInput}
                        defaultValue={name}
                    />
                    <input 
                        placeholder="Price" 
                        ref={priceInput => this.priceInput = priceInput}
                        defaultValue={price}
                    />
                    <button>Save</button>
                </form>
              )
              :(
                <div>
                    <span>{name}</span> 
                    {` | `} 
                    <span>{price}</span>
                    {` | `} 
                    <button onClick={this.onEdit.bind(this)}>Edit</button>
                    <button onClick={this.onDelete.bind(this)}>Delete</button>
                </div>
              )
          }
      </div>
    );
  }
}

export default ProductItem;
