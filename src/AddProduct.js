import React, { Component } from 'react';

class AddProduct extends Component {

    onSubmited(event){
        event.preventDefault();

        // console.log(this.nameInput.value, this.priceInput.value)
        this.props.onAdd(this.nameInput.value, this.priceInput.value);

        this.nameInput.value = '';
        this.priceInput.value = '';

    }
    render() {
        return(
            <form onSubmit={this.onSubmited.bind(this)}>
                <h3>Add Product</h3>
                <input placeholder="Name" ref={nameInput => this.nameInput = nameInput}/>
                <input placeholder="Price" ref={priceInput => this.priceInput = priceInput}/>
                <button>Submit</button>

                <hr/>
            </form>
        )
    }
}

export default AddProduct;