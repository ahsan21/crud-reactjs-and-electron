import React, { Component } from 'react';
import './App.css';
import ProductItem from './ProductItem';
import AddProduct from './AddProduct';

const products = [
  {
    name: 'iPad',
    price: 234
  },
  {
    name: 'iPhone',
    price: 456
  }
]

localStorage.setItem('products', JSON.stringify(products));

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      products: JSON.parse(localStorage.getItem('products'))
    };
  }

  componentWillMount(){
    const products = this.getProducts()

    this.setState({ products });
  }

  getProducts() {
    return this.state.products
    // console.log(products)
  }

  onAdd(name, price){
    const products = this.getProducts()

    products.push({ name, price });

    this.setState({ products });
  }

  onDelete(name){
    // console.log(name);
    const products = this.getProducts()

    const filteredProducts = products.filter(products => {
      return products.name !== name;
    })
    // console.log(filteredProducts)
    this.setState({ products: filteredProducts });
  }

  onEditSubmit(name, price, originalName) {
    // console.log(name, price)
    let products = this.getProducts()

    products = products.map(product => {
      if (product.name === originalName) {
        product.name = name;
        product.price = price;
      }

      return product;
    });

    this.setState({ products });

  }

  render() {
    return (
      <div className="App">
        <h1 className="App-intro">
          Lets make app crud example
        </h1>
        <AddProduct
          onAdd = {this.onAdd.bind(this)}
        />
        {
          this.state.products.map(products => {
            return (
              <ProductItem
                key={products.name}
                {...products}
                onDelete={this.onDelete.bind(this)}
                onEditSubmit={this.onEditSubmit.bind(this)}
              />
            )
          })
        }
      </div>
    );
  }
}

export default App;
